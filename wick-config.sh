#!/bin/bash

# Install Ansible
sudo apt install ansible

# Install Git 
git clone https://codeberg.org/kate/wick.git

# Go to Wick directory
cd wick

# Install Dependencies
ansible-galaxy collection install -r requirements.yml

# Copy the manifest json
cp manifest.yml.sample manifest.yml

# Update the system
sudo ansible-playbook playbooks/update.yml

# Config manifest
sudo ansible-playbook playbooks/configure.yml
