### Installation
1. Install Ansible via Terminal: ```sudo apt install ansible``` 

2. Download the Repository via Terminal: 
```git clone git@codeberg.org:kate/wick.git```

You can also download the repository via the ```Download``` Button. 

3. Go in tnto the folder (via graphical interface or via terminal with ```cd path/to/wick```)

4. The playbook and roles need some dependencies, which need to be installed.
(make sure you are in the ```wick```directory): ```ansible-galaxy collection install -r requirements.yml```

Now you have all the necessary software on your machine. You'll only need to configure and run it.

---
❗️❗️TL;DR: That still looks too complicated: **I'm a beginner!**❗️❗️
---
Ok. OK. You can also [download this bash script](https://codeberg.org/kate/wick/raw/branch/master/wick-config.sh). 

![Properties](https://codeberg.org/kate/wick/raw/branch/master/docs/shellscript_properties.png)

Right click "Properties", mark the checkbox "run as progam".

![Permission](https://codeberg.org/kate/wick/raw/branch/master/docs/shellscript_permission.png)

Open a terminal window (right click "Open Terminal") and type ``bash wick-config.sh`` and enter your user password.
