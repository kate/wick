# Workstation Installation & Configuration Kit  🧰

## Motivation

We don't want new Linux Users to be stressed and overwhelmed with their system.
In our experience, Linux Mint is the best software for beginners in the Linux world. Even though Linux Mint is easy to install, it is loaded with software. A lot of bloatware is pre-installed, but there is a lack of "useful" software. The script is supposed to streamline the installation and thus provide more security. At the same time, it installs software that can be useful for beginners. We have made sure that the software that is installed is significantly less than the software that is removed.  

## Description

A project for new Linux users or Linux beginners, who simply want the most important programs on their system to be present and set up and ready to work with.

> This software is distributed in the hope that it will be useful, but **WITHOUT ANY WARRANTY**; without even the implied warranty of **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.

## Features
- Updates your Linux system
- Removes a lot of bloatware from your system (e.g. games and other preinstalled software)
- Installs several helpful programs on your system. A complete list what will be installed or removed can be found here: https://codeberg.org/kate/wick/src/branch/master/roles/desktop_apps/defaults/main.yml)
- Configures your timezone
- Searches for the hidden Windows key on your system and saves it to a folder (just in case you need it someday)
- Starts nftables firewall
- Downloads a privacy documentation (german language)

### Requirements
- Supported Linux platform (Debian, Ubuntu, Linux Mint)
- Ansible setup (on terminal: ```sudo apt install ansible```)

### Testing
We have tested this script with Linux Mint 22 (xfce Edition).

### Changelog
See  [changelog.md](Changelog.md)

## Usage

### Installation
See  [installation.md](Installation.md)

#### Configuration

To configure the playbook, you need to create a `manifest.yml`. Therefore, copy the [manifest.yml.sample](./manifest.yml.sample) file and edit it to your
liking.

#### Copy the sample
```cp manifest.yml.sample manifest.yml```
(you can also do it with a simply copy&paste)

#### Open the file with your favorite text editor

A minimal example can look like the below snippet.

```
# ansible/manifest.yml
system:
  hostname: "localhost"
  timezone: "UTC"
  language: "de"
misc:
  store_msdm: true
documents:
  privacy_guide: true
```

## Run the playbook

Make sure you are in the ``wick`` directory. 

Open the terminal:

```sudo ansible-playbook playbooks/update.yml```

This updates your system (update OS, remove packages/bloatware, installs software)

```sudo ansible-playbook playbooks/configure.yml```

This configures your system.


## License
BSD 3-Clause License. Copyright (c) 2022, [Daniel Schier](https://gitlab.com/dschier) (https://gitlab.com/dschier), [Kate](https://codeberg.org) (https://codeberg.org/kate)

