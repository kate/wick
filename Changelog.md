## Changelog

# 29.08.2024
* added thingy and gnome-tweak-tool as removed desktop app
* changed firefox-esr to firefox
* added neovim, ripgrep, featherpad, rsync, metadata-cleaner, catfish, audacity, pdfarranger, tree, hashcat, darktable
* updated README.md
* created changelog.md
* deleted bookmark json template

# 30.08.2024 
* added bash script
* changed README.md

# 02.09.2024
* added webapp-manager, mint-backup, celluloid, simple-scan as, gucharmap, baobab (Disk Usage Analyzer), bulk-renamer, libreoffice-draw, xfce4-dict, brltty, mugshot, xfce4-dict, Xfce4-screenshooter as removed desktop app
* added Screenshots for README.md description
* changed README.md

